/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class conventionalbalancesheet {
	
	private String CapitalLiabilities;
	private String Amount1;
	private String Amount2;
	private String PropertyAssets;
	private String Amount3;
	private String Amount4;

	/**
	 * @param capitalLiabilities
	 * @param amount1
	 * @param amount2
	 * @param propertyAssets
	 * @param amount3
	 * @param amount
	 */
	public conventionalbalancesheet(String capitalLiabilities, String amount1,
			String amount2, String propertyAssets, String amount3, String amount4) {
		super();
		CapitalLiabilities = capitalLiabilities;
		Amount1 = amount1;
		Amount2 = amount2;
		PropertyAssets = propertyAssets;
		Amount3 = amount3;
		Amount4 = amount4;
	}

	/**
	 * @return the capitalLiabilities
	 */
	public String getCapitalLiabilities() {
		return CapitalLiabilities;
	}

	/**
	 * @return the amount1
	 */
	public String getAmount1() {
		return Amount1;
	}

	/**
	 * @return the amount2
	 */
	public String getAmount2() {
		return Amount2;
	}

	/**
	 * @return the propertyAssets
	 */
	public String getPropertyAssets() {
		return PropertyAssets;
	}

	/**
	 * @return the amount3
	 */
	public String getAmount3() {
		return Amount3;
	}

	/**
	 * @return the amount
	 */
	public String getAmount4() {
		return Amount4;
	}
	
}
