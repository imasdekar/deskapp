/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author girish
 *
 */
public class DateValidation {

	private static SimpleDateFormat dateformat ;
	
	static{
		
		dateformat = new SimpleDateFormat("yyyy-MM-dd");
		
	}
	public static boolean strToDate(String date)
	{
		@SuppressWarnings("unused")
		Date returndate=null;
		
		try {
			System.out.println("input is "+date);
			returndate = dateformat.parse(date);
			System.out.println("returning true");
			return true;
		}
		catch (ParseException e) {
			
			e.printStackTrace();
		}
		System.out.println("returning false");
		return false;
	}
	public static SimpleDateFormat getDateformat() {
		
		return dateformat;
	}
	public static void setDateformat(SimpleDateFormat dateformat) {
		
		DateValidation.dateformat = dateformat;
	}
	
	public static boolean isYearvalid(String from,String to)
	{
		try{
			 
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        	Date date1 = sdf.parse(from);
        	Date date2 = sdf.parse(to);
 
        	System.out.println(sdf.format(date1));
        	System.out.println(sdf.format(date2));
 
        	if(date1.compareTo(date2)>0){
        		System.out.println("Date1 is after Date2");
        		return false;
        	}else if(date1.compareTo(date2)<0){
        		System.out.println("Date1 is before Date2");
        		return true;
        	}else if(date1.compareTo(date2)==0){
        		System.out.println("Date1 is equal to Date2");
        		return false;
        	}else{
        		return false;
        	}
        	
    	}catch(ParseException ex){
    		ex.printStackTrace();
    	}
		
		 return false;
  	   
	}
	
	
	
}
